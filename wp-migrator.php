<?php
/**
 * WP Migrator Bootstrap
 *
 * @package     Grofftech\WpMigrator
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @copyright   2021 Grofftech
 * @license     GNU General Public License 2.0+
 *
 * @wordpress-plugin
 * Plugin Name: WP Migrator
 * Plugin URI:  https://
 * Description: Allows for database schema and data updates to be done with code.
 * Version:     1.1.1
 * Author:      Grofftech
 * Author URI:  https://grofftech.net
 * Text Domain: wp-migrator
 * License:     GPL-2.0+
 * License URI: http: //www.gnu.org/licenses/gpl-2.0.txt
 * Requires PHP: 7.2
 */

namespace Grofftech\WpMigrator;

use Grofftech\WpMigrator\Dependencies\Auryn\Injector;
use Grofftech\WpMigrator\Admin\Notification\Notification;

if ( ! defined( 'ABSPATH' ) ) {
    exit( "Not a valid WordPress Installation!" );
}

$plugin_path = \plugin_dir_path( __FILE__ );
$plugin_url = \plugin_dir_url( __FILE__ );

// Import and create the notification class
require_once $plugin_path . 'lib/Admin/Notification/Notification.php';
$notification = new Notification();

// Check PHP Version
if ( version_compare(PHP_VERSION, '7.2.0' ) <= 0 ) {
    $notification->show_error_message("Plugin Template requires PHP version 7.2.0 or higher." );

    deactivate_plugin();
}

// Autoload class files
$composer_autoload = "{$plugin_path}/vendor/autoload.php";

if ( ! file_exists( $composer_autoload ) ) {
    $notification->show_error_message( "Failed to load WP Migrator. Did you remember to run composer install?" );

    deactivate_plugin();
    return;
}

require $composer_autoload;

try {
    global $wp_migrator;
    $wp_migrator = new WpMigrator( $plugin_path, $plugin_url, new Injector() );

    \add_action('plugins_loaded', array( $wp_migrator, 'run') );

    \register_activation_hook(
        __FILE__,
        'Grofftech\WpMigrator\WpMigrator::activate'
    );

    \register_deactivation_hook(
        __FILE__,
        'Grofftech\WpMigrator\WpMigrator::deactivate'
    );

    \register_uninstall_hook(
        __FILE__,
        'Grofftech\WpMigrator\WpMigrator::uninstall'
    );

} catch( \Throwable $e) {
    $notification->show_error_message( "Failed to activate WP Migrator. {$e->getMessage()}" );

    deactivate_plugin();
}

/**
 * Deactivates the plugin.
 *
 * @since 1.0.0
 *
 * @return void
 */
function deactivate_plugin() {
    // This will hide the plugin activation notification
    unset( $_GET['activate'] );

    // Deactivate plugin
    \add_action( 'admin_init', function () {
        deactivate_plugins( __FILE__ );
	} );
}