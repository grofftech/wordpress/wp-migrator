# Changelog

## 1.1.0

- Updated namespace from GroffTech to Grofftech

## 1.0.4

- Update README
- Fix wrong namespace in the notification class

## 1.0.3

- Added vendor dependencies

## 1.0.2

- Added lib dependencies

## 1.0.1

- Added license configuration to composer.json

## 1.0.0

- Added running and rolling back migrations with WPCLI
