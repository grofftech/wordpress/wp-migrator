<?php
/**
 * File Service Tests
 *
 * @package     Grofftech\WpMigrator\Tests\Unit\FileService
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit\File;

use Grofftech\WpMigrator\Directory\DirectoryService;
use Grofftech\WpMigrator\File\FileService;
use Grofftech\WpMigrator\Tests\Unit\TestCase;

/**
 * File Service Tests class.
 */
class FileServiceTests extends TestCase {

    public function testShouldReturnAListOfFilesWhenGivenADirectory() {
        $path = WP_MIGRATOR_TESTS_DIRECTORY . '/File/Files';

        $file_service = new FileService( new DirectoryService() );
        $files = $file_service->get_files( $path );

        $this->assertNotEmpty( $files['filename'] );
        $this->assertContains( 'TestFile.php', $files['filename'] );
        $this->assertContains( 'TestFile2.php', $files['filename'] );
        $this->assertNotContains( "..", $files['filename'] );
        $this->assertNotContains( '.', $files['filename'] );
        $this->assertNotContains( 'TestDirectory', $files['filename'] );

        $this->assertNotEmpty( $files['filepath'] );
        $this->assertContains( $path . '/TestFile.php', $files['filepath'] );
        $this->assertContains( $path . '/TestFile2.php', $files['filepath'] );
    }
}
