<?php
/**
 * Unit Test bootstrap.
 *
 * @package     Grofftech\WpMigrator\Tests\Unit
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit;

define( 'WP_MIGRATOR_TESTS_DIRECTORY', __DIR__ );
define(
    'WP_MIGRATOR_PLUGIN_DIRECTORY',
    dirname( dirname( dirname( __DIR__ ) ) )
);

$composer_autoload = WP_MIGRATOR_PLUGIN_DIRECTORY . '/vendor/autoload.php';

if ( file_exists( $composer_autoload ) ) {
    require_once $composer_autoload;
}