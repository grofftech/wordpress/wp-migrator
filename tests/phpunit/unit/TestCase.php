<?php
/**
 * Test Case for unit tests
 *
 * @package Grofftech\WpMigrator\Tests\Unit
 * @author  Brett Groff <brett@grofftech.net>
 * @license GNU General Public License 2.0+
 * @link    https://grofftech.net
 * @since   1.0.0
 */

namespace Grofftech\WpMigrator\Tests\Unit;

use PHPUnit\Framework\TestCase as FrameworkTestCase;
use Brain\Monkey;
use Mockery;

/**
 * Test Case.
 */
abstract class TestCase extends FrameworkTestCase {
    /**
     * Prepares the test environment before each test.
     *
     * @return void
     */
    protected function setUp(): void {
        parent::setup();
        Monkey\setup();
    }

    /**
     * Cleans up the test environment after each test.
     *
     * @return void
     */
    protected function tearDown(): void {
        Monkey\tearDown();
        Mockery::close();
        parent::tearDown();
    }
}