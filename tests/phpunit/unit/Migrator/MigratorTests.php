<?php
/**
 * Migrator Tests
 * php version 7.3
 *
 * @category Blah
 * @package  Grofftech\WpMigrator\Tests\Unit\Migrator
 * @author   Brett Groff <brett@grofftech.net>
 * @license  GNU General Public License 2.0+
 * @link     https://grofftech.net
 * @since    1.0.0
 * 7.2.0
 */

namespace Grofftech\WpMigrator\Tests\Unit\Migrator;

use Grofftech\WpMigrator\Tests\Unit\TestCase;
use Brain\Monkey\Filters;
use Grofftech\WpMigrator\Domain\Migrator\Migrator;
use Mockery;

class MigratorTests extends MigratorTestsInitialize {

    /**
     * The default file info for testing.
     *
     * @var array
     */
    private $file_info;

    /**
     * Test setup.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function setUp() : void {
        parent::setup();
        parent::initialize();

        if ( ! defined( 'WP_MIGRATOR_DIR' ) ) {
            define( 'WP_MIGRATOR_DIR', '');
        }

        $this->file_info = array(
            'filename' => array(
                'Migration.php',
                'Migration2.php',
                'Migration3.php',
            ),
            'filepath' => array(
                '/var/www/html/wp-content/plugins/wp-migrator/tests/phpunit/unit/Migrator/TestMigrations/Migration.php',
                '/var/www/html/wp-content/plugins/wp-migrator/tests/phpunit/unit/Migrator/TestMigrations/Migration2.php',
                '/var/www/html/wp-content/plugins/wp-migrator/tests/phpunit/unit/Migrator/TestMigrations2/Migration3.php',
            )
        );
    }

    /**
     * Test get files is called twice when multiple paths are provided via a
     * wordpress filter.
     */
    public function testShouldCallGetFilesTwiceWhenMultiplePathsAreProvided() : void {
        $this->migration_repository_mock
            ->shouldReceive('get_existing_migrations')
            ->withNoArgs()
            ->andReturn(array());

        $this->migration_repository_mock
            ->shouldReceive('insert_migration');

        $this->reflection_service_mock
            ->shouldReceive('get_namespace_from_class_file')
            ->andReturnUsing(function ($argument) {
                if ( $this->file_info['filepath'][2] === $argument ) {
                    return 'Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations2';
                }
                else {
                    return 'Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations';
                }
            });

        $this->date_time_service_mock
            ->shouldReceive('get_date');

        Filters\expectApplied('wp_migrator_paths')
            ->once()
            ->andReturn( array(
                WP_MIGRATOR_TESTS_DIRECTORY . '/Migrator/TestMigrations',
                WP_MIGRATOR_TESTS_DIRECTORY . '/Migrator/TestMigrations2'
            ));

        // Assert.
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->twice()
            ->andReturn($this->file_info);

        // Action.
        $result = $this->migrator->run_migrations();

        // Assert.
        $this->assertEquals(3, $result['count']);
        $this->assertContains('Migration.php', $result['migrations']);
        $this->assertContains('Migration2.php', $result['migrations']);
        $this->assertContains('Migration3.php', $result['migrations']);
    }

    /**
     * Test exception is thrown when no migration files exist.
     */
    public function testShouldThrowExceptionWhenNoMigrationFilesAreFound() {
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn(array());

        // Assert.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('There are no migrations to run');

        // Action.
        $this->migrator->run_migrations();
    }

    /**
     * Test only runs migrations that have not been run before.
     */
    public function testShouldRunMigrationsThatHaveNotAlreadyBeenRun() {
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $this->reflection_service_mock
            ->shouldReceive('get_namespace_from_class_file')
            ->andReturnUsing(function ($argument) {
                if ( $this->file_info['filepath'][2] === $argument ) {
                    return 'Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations2';
                }
                else {
                    return 'Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations';
                }
            });

        $this->date_time_service_mock
            ->shouldReceive('get_date')
            ->with(\Mockery::any())
            ->andReturn('2021-08-25 00:00:00');

        // Excludes Migration.php from being run.
        $this->migration_repository_mock
            ->shouldReceive('get_existing_migrations')
            ->andReturn(array('Migration.php'));

        // Assert.
        $this->migration_repository_mock
            ->shouldReceive('insert_migration')
            ->with(\Mockery::on(function ($argument) {
                $filename = $argument['name'];
                $date = $argument['run_date'];

                if ($this->file_info['filename'][1] === $filename
                    && '2021-08-25 00:00:00' === $date) {
                        return true;
                    }
            }));

        // Assert.
        $this->migration_repository_mock
            ->shouldReceive('insert_migration')
            ->with(\Mockery::on(function ($argument) {
                $filename = $argument['name'];
                $date = $argument['run_date'];

                if ($this->file_info['filename'][2] === $filename
                    && '2021-08-25 00:00:00' === $argument['run_date']) {
                        return true;
                    }
            }));

        // Action.
        $result = $this->migrator->run_migrations();

        // Assert.
        $this->assertEquals(2, $result['count']);
        $this->assertContains('Migration2.php', $result['migrations']);
        $this->assertContains('Migration3.php', $result['migrations']);
    }

    /**
     * Test should run migration by name when it has not been run before.
     */
    public function testShouldRunMigrationByNameWhenItHasNotBeenRun() {
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $this->migration_repository_mock
            ->shouldReceive('get_migration')
            ->with('Migration.php')
            ->andReturn(new \stdClass());

        $this->reflection_service_mock
            ->shouldReceive('get_namespace_from_class_file')
            ->with(\Mockery::any())
            ->andReturn(
                'Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations'
            );

        $this->date_time_service_mock
            ->shouldReceive('get_date')
            ->with(\Mockery::any())
            ->andReturn('2021-08-25 00:00:00');

        // Assert.
        $this->migration_repository_mock
            ->shouldReceive('insert_migration')
            ->once();

        // Action.
        $result = $this->migrator->run_migration('Migration.php');

        $this->assertEquals('Migration.php', $result);
    }

    public function testShouldThrowErrorWhenRequestingMigrationByNameAndFileIsNotFound() {
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        // Assert.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(
            'Migration for Migration4.php does not exist.'
        );

        // Action.
        $this->migrator->run_migration('Migration4.php');
    }

    public function testShouldThrowErrorWhenRequestingMigrationByNameAndItHasAlreadyRun() {
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $result = new \stdClass();
        $result->name = 'Migration.php';
        $this->migration_repository_mock
            ->shouldReceive('get_migration')
            ->with(\Mockery::any())
            ->andReturn($result);

        // Assert.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(
            'Migration.php has already been run.'
        );

        // Action.
        $this->migrator->run_migration('Migration.php');
    }

    public function testShouldThrowErrorIfMigrationIsAlreadyRolledBack() {
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $result = new \stdClass();
        $result->name = 'Migration.php';
        $result->is_rollback = '1';
        $this->migration_repository_mock
            ->shouldReceive('get_migration')
            ->with(\Mockery::any())
            ->andReturn($result);

        // Assert.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('A migration for Migration.php cannot be rolled back or has already been rolled back');

        // Action.
        $this->migrator->rollback_migration('Migration.php');
    }

    public function testShouldThrowErrorWhenThereIsNoRollbackMethodForRegularClass() {
        $this->file_info = array(
            'filename' => array(
                'MigrationNoRollback.php'
            ),
            'filepath' => array(
                '/var/www/html/wp-content/plugins/wp-migrator/tests/phpunit/unit/Migrator/TestMigrations/MigrationNoRollback.php',
            )
        );

        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $result = new \stdClass();
        $result->name = 'MigrationNoRollback.php';
        $result->is_rollback = '0';
        $this->migration_repository_mock
            ->shouldReceive('get_migration')
            ->with('MigrationNoRollback.php')
            ->andReturn($result);

        $this->reflection_service_mock
            ->shouldReceive('get_reflection_class')
            ->with(\Mockery::any())
            ->andReturn(new \ReflectionClass('Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations\MigrationNoRollback'));

        $this->reflection_service_mock
            ->shouldReceive('get_namespace_from_class_file')
            ->with(\Mockery::any())
            ->andReturn('Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations');

        // Assert.
        $this->expectException(\Exception::class);

        // Action.
        $this->migrator->rollback_migration('MigrationNoRollback.php');
    }

    public function testShouldThrowErrorWhenThereIsNoRollbackForInheritanceClass() {
        $this->file_info = array(
            'filename' => array(
                'MigrationNoRollbackWithInheritance.php'
            ),
            'filepath' => array(
                '/var/www/html/wp-content/plugins/wp-migrator/tests/phpunit/unit/Migrator/TestMigrations/MigrationNoRollbackWithInheritance.php',
            )
        );

        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $result = new \stdClass();
        $result->name = 'MigrationNoRollback.php';
        $result->is_rollback = '0';
        $this->migration_repository_mock
            ->shouldReceive('get_migration')
            ->with('MigrationNoRollbackWithInheritance.php')
            ->andReturn($result);

        $this->reflection_service_mock
            ->shouldReceive('get_reflection_class')
            ->with(\Mockery::any())
            ->andReturn(new \ReflectionClass('Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations\MigrationNoRollbackWithInheritance'));

        $this->reflection_service_mock
            ->shouldReceive('get_namespace_from_class_file')
            ->with(\Mockery::any())
            ->andReturn('Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations');

        // Assert.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('There is no rollback for MigrationNoRollbackWithInheritance.php');

        // Action.
        $this->migrator->rollback_migration(
            'MigrationNoRollbackWithInheritance.php'
        );
    }

    public function testShouldRollbackMigrationWhenItHasAlreadyBeenRun() {
        $this->file_info = array(
            'filename' => array(
                'MigrationWithRollback.php'
            ),
            'filepath' => array(
                '/var/www/html/wp-content/plugins/wp-migrator/tests/phpunit/unit/Migrator/TestMigrations/MigrationWithRollback.php',
            )
        );

        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $result = new \stdClass();
        $result->name = 'MigrationWithRollback.php';
        $result->is_rollback = '0';
        $this->migration_repository_mock
            ->shouldReceive('get_migration')
            ->with('MigrationWithRollback.php')
            ->andReturn($result);

        $this->reflection_service_mock
            ->shouldReceive('get_reflection_class')
            ->with(\Mockery::any())
            ->andReturn(new \ReflectionClass('Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations\MigrationWithRollback'));

        $this->reflection_service_mock
            ->shouldReceive('get_namespace_from_class_file')
            ->with(\Mockery::any())
            ->andReturn('Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations');

        $this->migration_repository_mock
            ->shouldReceive('update_migration')
            ->with(\Mockery::any(), \Mockery::any());

        // Action.
        $result =
            $this->migrator->rollback_migration('MigrationWithRollback.php');

        $this->assertEquals('MigrationWithRollback.php', $result);
    }

    public function testShouldThrowErrorWhenMigrationClassHasNoNamespace() {
        $this->file_service_mock
            ->shouldReceive('get_files')
            ->with(\Mockery::any())
            ->andReturn($this->file_info);

        $this->migration_repository_mock
            ->shouldReceive('get_migration')
            ->with('Migration.php')
            ->andReturn(new \stdClass());

        $this->reflection_service_mock
            ->shouldReceive('get_namespace_from_class_file')
            ->with(\Mockery::any())
            ->andReturn('');

        // Assert.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Namespace is required in Migration.php');

        // Action.
        $this->migrator->run_migration('Migration.php');
    }
}