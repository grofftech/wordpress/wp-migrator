<?php
/**
 * Abstract Migrations
 *
 * @package     Grofftech\WpMigrator\Migrations
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit\Migrator\Abstracts;

/**
 * Migration class.
 */
abstract class AbstractMigration
{
    /**
     * Runs the migration.
     */
    abstract protected function run();

    /**
     * Rollback a migration.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function rollback() {

    }
}