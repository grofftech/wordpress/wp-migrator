<?php
/**
 * Migration 3
 *
 * @package     Grofftech\WpMigrator\Tests\Migrator\TestMigrations2
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations2;

/**
 * Migration 3 class.
 */
class Migration3 {

    /**
     * Constructor
     */
    public function __construct() {

    }

    public function run() {

    }
}