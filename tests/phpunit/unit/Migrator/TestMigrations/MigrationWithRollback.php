<?php
/**
 * Migration
 *
 * @package     Grofftech\WpMigrator\Tests\Migrator\TestMigrations
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit\Migrator\TestMigrations;

/**
 * Migration class.
 */
class MigrationWithRollback {

    /**
     * Constructor
     */
    public function __construct() {

    }

    public function run() : void {

    }

    public function rollback() : void {

    }
}