<?php
/**
 * Migrator Tests Initialize
 *
 * @package     Grofftech\WpMigrator\Tests\Unit\Migrator
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit\Migrator;

use Grofftech\WpMigrator\Domain\Migrator\Migrator;
use Grofftech\WpMigrator\Tests\Unit\TestCase;

/**
 * Migrator Tests Initialize class.
 */
class MigratorTestsInitialize extends TestCase {
    /**
     * The default namespace.
     *
     * @var string
     */
    private $namespace = 'Grofftech\WpMigrator\\';

    /**
     * The file service mock.
     *
     * @var \Mockery
     */
    protected $file_service_mock;

    /**
     * The directory service mock.
     *
     * @var \Mockery
     */
    protected $directory_service_mock;

    /**
     * The reflection service mock.
     *
     * @var \Mockery
     */
    protected $reflection_service_mock;

    /**
     * The migration repository mock.
     *
     * @var \Mockery
     */
    protected $migration_repository_mock;

    /**
     * The date time service mock.
     *
     * @var \Mockery
     */
    protected $date_time_service_mock;

    /**
     * The migrator class (system under test)
     *
     * @var Grofftech\WpMigrator\Domain\Migrator\Migrator
     */
    protected $migrator;

    /**
     * Initializes the mocks as run time partials
     * and class (system under test).
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function initialize() : void {
        $this->file_service_mock =
            \Mockery::mock( $this->namespace . 'File\Interfaces\FileServiceInterface')
                ->makePartial();

        $this->directory_service_mock =
            \Mockery::mock( $this->namespace . 'Directory\Interfaces\DirectoryServiceInterface')
                ->makePartial();

        $this->reflection_service_mock =
            \Mockery::mock( $this->namespace . 'Reflection\Interfaces\ReflectionServiceInterface')
                ->makePartial();

        $this->migration_repository_mock =
            \Mockery::mock( $this->namespace . 'Domain\Interfaces\MigrationRepositoryInterface')
                ->makePartial();

        $this->date_time_service_mock =
            \Mockery::mock( $this->namespace . 'DateTime\Interfaces\DateTimeServiceInterface')
                ->makePartial();

        $this->migrator = new Migrator(
            $this->file_service_mock,
            $this->migration_repository_mock,
            $this->reflection_service_mock,
            $this->date_time_service_mock
        );
    }
}