<?php
/**
 * Reflection Service Tests
 *
 * @package     Grofftech\WpMigrator\Tests\Unit\Reflection
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit\Reflection;

use Grofftech\WpMigrator\Reflection\ReflectionService;
use Grofftech\WpMigrator\Tests\Unit\TestCase;
use Grofftech\WpMigrator\Tests\Unit\Reflection\Files\TestFile;

/**
 * Reflection Service Tests class
 */
class ReflectionServiceTests extends TestCase {

    public function testShouldReturnNamespaceFromFileWhenFound() {
        $expected_namespace =
            "Grofftech\WpMigrator\Tests\Unit\Reflection\Files";

        $path_with_filename = WP_MIGRATOR_TESTS_DIRECTORY . '/Reflection/Files/TestFile.php';

        // Action
        $reflection_service = new ReflectionService();
        $namespace = $reflection_service
                        ->get_namespace_from_class_file($path_with_filename );

        $this->assertEquals( $expected_namespace, $namespace );
    }

    public function testShouldReturnClassAsReflectionClass() {
        $class = new TestFile();

        // Action
        $reflection_service = new ReflectionService();
        $reflection_class = $reflection_service->get_reflection_class( $class );

        $this->assertIsObject($reflection_class);
    }
}