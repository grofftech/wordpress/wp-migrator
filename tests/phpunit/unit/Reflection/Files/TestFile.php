<?php
/**
 * Test File for parsing.
 *
 * @package     Grofftech\WpMigrator\Tests\Unit\Reflection
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Tests\Unit\Reflection\Files;

/**
 * Test File class.
 */
class TestFile {

    /**
     * Constructor
     */
    public function __construct() {

    }

}
