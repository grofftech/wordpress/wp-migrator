<?php
/**
 * Migration Repository Interface
 *
 * @package     Grofftech\WpMigrator\Repositories\Migration\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Domain\Interfaces;

/**
 * Migration Repository interface.
 */
interface MigrationRepositoryInterface {
    public function get_existing_migrations();
    public function get_migration( $name );
    public function insert_migration( array $args );
    public function update_migration( array $args, array $filter );
}