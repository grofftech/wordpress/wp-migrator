<?php
/**
 * Migrator
 *
 * @package     Grofftech\WpMigrator\Migrator
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Domain\Migrator;

use Exception;
use Grofftech\WpMigrator\DateTime\Interfaces\DateTimeServiceInterface;
use Grofftech\WpMigrator\File\Interfaces\FileServiceInterface;
use Grofftech\WpMigrator\Domain\Interfaces\MigrationRepositoryInterface;
use Grofftech\WpMigrator\Reflection\Interfaces\ReflectionServiceInterface;
use Grofftech\WpMigrator\Service\Service;
use Grofftech\WpMigrator\Utilities\StringHelpers;

/**
 * Migrator class.
 */
class Migrator extends Service {
    /**
     * The table name to keep track of migrations.
     *
     * @var string
     */
    protected static $table_name = 'migrations';

    /**
     * File Service.
     *
     * @var FileServiceInterface
     */
    private $file_service;

    /**
     * Migration Repository.
     *
     * @var MigrationRepositoryInterface
     */
    private $migration_repository;

    /**
     * Reflection Service.
     *
     * @var ReflectionServiceInterface
     */
    private $reflection_service;

    /**
     * Date Time Service
     *
     * @var DateTimeServiceInterface
     */
    private $date_time_service;

    /**
     * Constructor
     */
    public function __construct( FileServiceInterface $file_Service,
                                 MigrationRepositoryInterface $migration_repository,
                                 ReflectionServiceInterface $reflection_service,
                                 DateTimeServiceInterface $date_time_service) {
        $this->file_service = $file_Service;
        $this->migration_repository = $migration_repository;
        $this->reflection_service = $reflection_service;
        $this->date_time_service = $date_time_service;
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() : void {

    }

    /**
     * Creates the table that keeps track of migrations.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function create_migrations_table() : void {
        global $wpdb;

        $table = $wpdb->prefix . self::$table_name;

        $collation = ! $wpdb->has_cap( 'collation' )
            ? ''
            : $wpdb->get_charset_collate();

		$sql = "CREATE TABLE IF NOT EXISTS " . $table . " (
			id bigint(20) NOT NULL auto_increment,
			name varchar(255) NOT NULL,
			run_date datetime DEFAULT NULL,
            is_rollback bit NOT NULL DEFAULT 0,
			PRIMARY KEY (id)
			) {$collation};";

        // Gives access to the dbDelta function
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        \dbDelta( $sql );
    }

    /**
     * Removes the table that keeps track of migrations.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function drop_migrations_table() : void {
        global $wpdb;

        $table = $wpdb->prefix . self::$table_name;

        $sql = "DROP TABLE IF EXISTS " . $table;

        $wpdb->query( $sql );
    }

    /**
     * Runs all pending migrations.
     *
     * @since 1.0.0
     *
     * @return array Data about the migrations run.
     */
    public function run_migrations() : array {
        $migration_count = 0;
        $results = array(
            'count' => $migration_count,
            'migrations' => array()
        );

        $migrations = $this->get_migrations();

        // No files means no migrations to run
        if ( 0 === count( $migrations ) ) {
            throw new Exception( 'There are no migrations to run' );
        }

        // Get the existing migrations run from the database
        $existing_migrations =
            $this->migration_repository->get_existing_migrations();

        // Run the migrations that have yet to run
        $migrations_count = count( $migrations['filename'] );
        for ( $i = 0; $i < $migrations_count; $i++) {
            if ( in_array( $migrations['filename'][$i], $existing_migrations ) ) {
                continue;
            }

            $class = $this->get_class_name(
                $migrations['filepath'][$i],
                $migrations['filename'][$i]
            );

            $migration = new $class;
            $migration->run();

            $this->migration_repository->insert_migration( array(
                'name' => $migrations['filename'][$i],
                'run_date' => $this->date_time_service->get_date("Y-m-d H:i:s")
            ) );

            array_push( $results['migrations'], $migrations['filename'][$i] );
            ++$migration_count;
            $results['count'] = $migration_count;
        }

        return $results;
    }

    /**
     * Run a migration based on a filename.
     *
     * @since 1.0.0
     *
     * @param string $name The name of the
     *                     migration file (e.g. TestMigration.php)
     *
     * @return void
     */
    public function run_migration( string $name ) : string {
        $migration_file = $this->get_migration( $name );

        $existing_migration =
            $this->migration_repository->get_migration( $name );

        if ( $this->is_migration_run( $name, $existing_migration ) ) {
            throw new \Exception( "{$name} has already been run." );
        }

        $class = $this->get_class_name(
            $migration_file['filepath'][0],
            $migration_file['filename'][0]
        );

        $migration = new $class;
        $migration->run();

        $this->migration_repository->insert_migration( array(
            'name' => $migration_file['filename'][0],
            'run_date' => $this->date_time_service->get_date("Y-m-d H:i:s")
        ) );

        return $migration_file['filename'][0];
    }

    /**
     * Rollback a migration by name.
     *
     * @since 1.0.0
     *
     * @param string $name The name of the migration
     *                     file (e.g. TestMigration.php)
     *
     * @return string
     */
    public function rollback_migration( string $name ) : string {
        $migration_file = $this->get_migration( $name );

        $existing_migration =
            $this->migration_repository->get_migration( $name );

        if ( $this->is_migration_rolled_back( $existing_migration ) ) {
            throw new \Exception( "A migration for {$name} cannot be rolled back or has already been rolled back" );
        }

        $class = $this->get_class_name(
            $migration_file['filepath'][0],
            $migration_file['filename'][0]
        );

        $migration = new $class;

        $reflection_class =
            $this->reflection_service->get_reflection_class( $migration );

        // This will tell us which class has declared it
        // (either base or derived)
        $rollback_method = $reflection_class->getMethod('rollback');

        if ( StringHelpers::string_has_substring( $rollback_method->class, "AbstractMigration" ) ) {
            throw new \Exception( "There is no rollback for {$name}" );
        }

        $migration->rollback();

        $data = array(
            'is_rollback' => 1
        );

        $filter = array(
            'column' => 'name',
            'comparison' => '=',
            'value' => $migration_file['filename'][0]
        );

        $this->migration_repository->update_migration(
            $data,
            $filter
        );

        return $migration_file['filename'][0];
    }

    /**
     * Checks to see if a migration has been rolled back.
     *
     * @since 1.0.0
     *
     * @param object $migration The migration from the database.
     *
     * @return bool
     */
    private function is_migration_rolled_back( object $migration ) {
        return '1' === $migration->is_rollback;
    }

    /**
     * Checks to see if a migration has already been run.
     *
     * @since 1.0.0
     *
     * @param string $name The name of the migration file (e.g Migration.php)
     * @param object $migration The migration from the database.
     *
     * @return bool
     */
    private function is_migration_run( string $name, object $migration ) {
        return property_exists( $migration, "name")
               && $name === $migration->name;
    }

    /**
     * Gets the class name with namespace.
     *
     * @since 1.0.0
     *
     * @param string $path      The filepath of the class file.
     * @param string $filename  The filename of the class file.
     *
     * @return string The fully qualified class name with the namespace.
     */
    private function get_class_name( $path, $filename ) : string {
        $namespace = $this
            ->reflection_service
            ->get_namespace_from_class_file( $path );

        if ( '' === $namespace ) {
            throw new \Exception( "Namespace is required in {$filename}" );
        }
        $file = basename( $filename, '.php' );

        return $namespace . '\\' . $file;
    }

    /**
     * Get the migration files.
     *
     * @since 1.0.0
     *
     * @return array An array of file information for the migration
     * files
     */
    private function get_migrations() : array {
        $paths = \apply_filters(
            'wp_migrator_paths',
            array( WP_MIGRATOR_DIR . 'lib/Migrations' )
        );

        $migrations = array();
        foreach ( $paths as $path ) {
            $migration_files = $this->file_service->get_files( $path );
            $migrations = array_merge( $migrations, $migration_files );
        }

        return $migrations;
    }

    /**
     * Get a migration by name.
     *
     * @since 1.0.0
     *
     * @param string $name The filename of the migration.
     *
     * @return array
     */
    private function get_migration( $name ) : array {
        $migration = array();
        $migrations = $this->get_migrations();

        if ( ! in_array( $name, $migrations['filename'] ) ) {
            throw new \Exception( "Migration for {$name} does not exist." );
        }
        else {
            $migration['filename'][] = $name;
        }

        foreach ( $migrations['filepath'] as $filepath ) {
            if ( strpos( $filepath, $name ) != false ) {
                $migration['filepath'][] = $filepath;
                break;
            }
        }

        return $migration;
    }
}