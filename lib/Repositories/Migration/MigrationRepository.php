<?php
/**
 * Migration Repository
 *
 * @package     Grofftech\WpMigrator\Repositories\Migration
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Repositories\Migration;

use Grofftech\WpMigrator\Repositories\SqlRepository;
use Grofftech\WpMigrator\Domain\Interfaces\MigrationRepositoryInterface;
use stdClass;

/**
 * Migration Repository class.
 */
class MigrationRepository extends SqlRepository implements MigrationRepositoryInterface {
    private $table_name = 'migrations';

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Gets the names of migration files.
     *
     * @since 1.0.0
     *
     * @return array
     */
    public function get_existing_migrations() : array {
        $sql = "";

        $sql = $this->build_select( $this->table_name, array( 'name' ) );
        return $this->get_column( $sql );
    }

    /**
     * Get a specific migration name.
     *
     * @param string $name The name of the migration file.
     *
     * @since 1.0.0
     *
     * @return array
     */
    public function get_migration( $name ) : object {
        $sql = "";

        $filter = array(
            'column' => 'name',
            'value' => $name,
            'comparison' => "="
        );

        $sql = $this->build_select_where(
            $this->table_name,
            array ( 'name', 'is_rollback' ),
            $filter
        );

        $result = new stdClass();

        $data = $this->query_get_results( $sql );
        if ( count( $data ) > 0 ) {
            $result = $data[0];
        }

        return $result;
    }

    /**
     * Inserts a migration.
     *
     * @since 1.0.0
     *
     * @param array $args The data for the insert (columns & values).
     *
     * @return void
     */
    public function insert_migration( array $args ) : void {
        $sql = $this->build_insert( $this->table_name, $args );
        $this->query( $sql );
    }

    /**
     * Updates a migration.
     *
     * @since 1.0.0
     *
     * @param array $args The data for the delete.
     *
     * @return void
     */
    public function update_migration( array $args, array $filter ) : void {
        $sql = $this->build_update( $this->table_name, $args, $filter );
        $this->query( $sql );
    }
}