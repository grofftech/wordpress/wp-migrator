<?php
/**
 * Sql Repository
 *
 * @package     Grofftech\WpMigrator\Repositories
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Repositories;

use InvalidArgumentException;

/**
 * Base Repository class
 */
abstract class SqlRepository {

    private $db;

    /**
     * Constructor
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
    }

    /**
     * Query and get the results
     *
     * @param string $sql           The sql to use.
     * @param string $output_type   The output type. Defaults to object.
     *                              Other output types are ARRAY_A |
     *                              ARRAY_N | OBJECT_K
     *
     * @since 1.0.0
     *
     * @return array|object
     */
    protected function query_get_results( string $sql, $output_type = OBJECT ) {
        return $this->db->get_results( $sql, $output_type );
    }

    /**
     * Query with no results.
     *
     * @param string $sql The sql to use.
     *
     * @since 1.0.0
     *
     * @return int|bool
     */
    protected function query( string $sql ) {
        return $this->db->query( $sql );
    }

    /**
     * Query and get all column values.
     *
     * @param string $sql The sql to use.
     *
     * @since 1.0.0
     *
     * @return array
     */
    protected function get_column( string $sql ) : array {
        return $this->db->get_col( $sql );
    }

    /**
     * Builds the sql select statement.
     *
     * @param string $table_name The table name.
     * @param array $columns     The columns to select.
     *
     * @since 1.0.0
     *
     * @return string
     */
    protected function build_select( string $table_name, array $columns = array() ) : string {
        $sql = "";

        // Get all columns
        if ( empty( $columns ) ) {
            return "SELECT * FROM {$this->db->prefix}{$table_name};";
        }

        // Get specific columns
        $sql = "SELECT ";

        $column_count = count( $columns );
        for ( $i = 0; $i < $column_count; $i++ ) {
            if ( $column_count - 1 === $i ) {
                $sql .= "{$columns[$i]}";
            }
            else {
                $sql .= "{$columns[$i]},";
            }
        }

        $sql .= " FROM {$this->db->prefix}{$table_name};";

        return $sql;
    }

    /**
     * Builds the sql select statement with a where clause.
     *
     * @param string $table_name The table name.
     * @param array  $columns    The columns to select.
     * @param array  $filter     The data to be filtered.
     *
     * @since 1.0.0
     *
     * @return string
     */
    protected function build_select_where( string $table_name, array $columns = array(), array $filter ) : string {
        $sql = '';

        if ( empty( $filter ) ) {
            throw new InvalidArgumentException( "Filter for where clause required" );
        }

        if ( empty( $columns ) ) {
            $sql .= "SELECT * ";
        }
        else {
            $sql .= "SELECT ";

            $count = count( $columns );
            for ( $i = 0; $i < $count; $i++ ) {
                if ( $count - 1 === $i ) {
                    $sql .= "{$columns[$i]}";
                }
                else {
                    $sql .= "{$columns[$i]},";
                }
            }
        }

        $sql .= " FROM {$this->db->prefix}{$table_name} ";

        $where = $this->build_where( $filter );
        $sql .= $where['sql'];

        $sql .= ";";

        return $this->prepare( $sql, $where['placeholders'] );
    }

    /**
     * Builds the sql insert statement
     *
     * @param string $table_name The table name.
     * @param array  $data       The data for the insert, including column
     *                           names and values
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function build_insert( string $table_name, array $data ) : string {
        if ( ! isset( $data ) ) {
            throw new InvalidArgumentException( "Data required for insert" );
        }

        $sql = "INSERT INTO {$this->db->prefix}{$table_name} ";
        $sql .= "( ";

        $count = count( $data );
        $columns = array_keys( $data );
        $values = array_values( $data );

        for ( $i = 0; $i < $count; $i++ ) {
            if ( $count - 1 === $i ) {
                $sql .= "{$columns[$i]} ";
            }
            else {
                $sql .= "{$columns[$i]}, ";
            }
        }
        $sql .= ") ";
        $sql .= "VALUES ( ";

        for ( $i = 0; $i < $count; $i++ ) {
            if ( $count - 1 === $i ) {
                $sql .= "'{$values[$i]}' ";
            }
            else {
                $sql .= "'{$values[$i]}', ";
            }
        }

        $sql .= ");";

        return $sql;
    }

    public function build_update( string $table_name, array $data, array $filter ) : string {
        if ( ! isset( $data ) ) {
            throw new InvalidArgumentException( "Data required for update" );
        }

        $sql = "UPDATE {$this->db->prefix}{$table_name} ";
        $sql .= "SET ";

        $count = count( $data );
        $columns = array_keys( $data );
        $values = array_values( $data );

        for ( $i = 0; $i < $count; $i++ ) {
            if ( $count - 1 === $i ) {
                $sql .= "{$columns[$i]} = {$values[$i]} ";
            }
            else {
                $sql .= "{$columns[$i]} = {$values[$i]}, ";
            }
        }

        $where = $this->build_where( $filter );
        $sql .= $where['sql'];

        $sql .= ';';

        return $this->prepare( $sql, $where['placeholders'] );
    }

    /**
     * Builds the sql delete statement.
     *
     * @param string $table_name The table name.
     * @param array  $filter     The data to be filtered.
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function build_delete( $table_name, array $filter ) : string {
        if ( ! isset( $filter ) ) {
            throw new InvalidArgumentException( "Filter required" );
        }

        $sql = "DELETE FROM {$table_name} ";
        $sql .= $this->build_where( $filter );

        return $sql;
    }

    /**
     * Builds the where clause.
     *
     * @since 1.0.0
     *
     * @param array $filter The data to be filtered.
     *
     * @return array An array of sql and it's associated placeholders
     */
    private function build_where( array $filter ) : array {
        $placeholders = array();

        $sql = "WHERE ";

        if ( ! array_key_exists( 'relation', $filter ) ) {
            $sql .= "{$filter['column']} ";
            $sql .= "{$filter['comparison']} ";
            $sql .= "{$this->create_placeholder( $filter['value'] )}";

            $placeholders[] = $filter['value'];
        }
        else {
            // TODO: How to design args coming in for parsing when it's relation (AND/OR)
        }

        $result = array();
        $result['sql'] = $sql;
        $result['placeholders'] = $placeholders;

        return $result;
    }

    /**
     * Creates placeholder for sql parameters.
     *
     * @param string $data The value to be replaced with a placeholder .
     *
     * @since 1.0.0
     *
     * @return string
     */
    private function create_placeholder( $data ) : string {
        if ( is_string( $data ) ) {
            return '%s';
        }

        if ( is_int( $data ) ) {
            return '%d';
        }

        if ( is_float( $data ) ) {
            return '%f';
        }
    }

    /**
     * Prepares sql by sanitizing values.
     *
     * @param string $sql The sql to use.
     * @param array  $placeholders The list of placeholder values.
     *
     * @since 1.0.0
     *
     * @return string
     */
    private function prepare( $sql, array $placeholders ) : string {
        if ( ! is_array( $placeholders ) ) {
            throw new InvalidArgumentException( "Placeholders argument must be an array" );
        }

        $args = implode(',', $placeholders );
        return $this->db->prepare( $sql, $args );
    }
}
