<?php
/**
 * Date Time Service
 *
 * @package     Grofftech\WpMigrator\DateTime
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\DateTime;

use Grofftech\WpMigrator\DateTime\Interfaces\DateTimeServiceInterface;
use Grofftech\WpMigrator\Service\Service;

/**
 * Date Time Service class.
 */
class DateTimeService extends Service implements DateTimeServiceInterface {

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() : void {

    }

    /**
     * Gets the current date.
     *
     * @param string $format    The date format to use.
     * @param int    $timestamp The unix timestamp.
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function get_date( string $format, ?int $timestamp = null ) : string {
        if ( null === $timestamp ) {
            return date( $format );
        }

        return date( $format, $timestamp );
    }
}