<?php
/**
 * Date Time Interface
 *
 * @package     Grofftech\WpMigrator\DateTime\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\DateTime\Interfaces;

/**
 * Date Time Service interface.
 */
interface DateTimeServiceInterface {
    public function get_date( string $format, ?int $timestamp = null );
}