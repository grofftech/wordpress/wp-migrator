<?php
/**
 * Hookable Interface
 *
 * @package     Grofftech\WpMigrator\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Interfaces;

/**
 * Hookable interface.
 */
interface Hookable {
    /**
     * Register hooks.
     */
    public function register_hooks();
}