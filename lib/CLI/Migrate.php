<?php
/**
 * Migrate Command for WP-CLI
 *
 * @package     Grofftech\WpMigrator\CLI
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\CLI;

use Grofftech\WpMigrator\Domain\Migrator\Migrator;
use Grofftech\WpMigrator\Utilities\StringHelpers;

/**
 * Performs database migrations.
 */
class Migrate {
    /**
     * The migrator object.
     *
     * @var Migrator
     */
    private $migrator;

    /**
     * Constructor
     *
     * @param Migrator $migrator The migrator object.
     */
    public function __construct( Migrator $migrator ) {
        $this->migrator = $migrator;
    }

    /**
     * Initializes the migrate command with wp-cli.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function init() : void {
        global $wp_migrator;

        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            \WP_CLI::add_command(
                'migrate',
                $wp_migrator->get_class( 'Grofftech\WpMigrator\CLI\Migrate' )
            );
        }
    }

    /**
     * Runs all pending database migrations.
     *
     * @since 1.0.0
     *
     * @param array $args.      The command line arguments from wp-cli
     *                          that do not have --
     * @param array $assoc_args The command line arguments from wp-cli
     *                          that have --
     *
     * @return void
     */
    public function run( $args, $assoc_args ) : void {
        try {
            // No --arguments have been provided, runs all pending migrations
            if ( empty( $assoc_args ) ) {
                $results = $this->migrator->run_migrations();

                if ( 0 === $results['count'] ) {
                    \WP_CLI::line( 'There are no migrations to run.' );
                } else {
                    $message =
                        "Successfully ran {$results['count']} migrations:\n";

                    $migrations = $results['migrations'];
                    if ( ! empty( $migrations ) ) {
                        foreach ( $migrations as $migration => $name ) {
                            if ( $migration === array_key_last( $migrations ) ) {
                                $message .= "{$name}";
                            }
                            else {
                                $message .= "{$name}\n";
                            }
                        }
                    }

                    \WP_CLI::success($message);
                }
            }

            // Run a specific migration
            if ( isset( $assoc_args['name'] )
                && $assoc_args['name'] ) {
                $migration = $assoc_args['name'];

                if ( StringHelpers::string_ends_with( $migration, ".php" ) ) {
                    throw new \Exception(
                        "Please provide the migration name without the .php extension"
                    );
                }

                $this->migrator->run_migration( "{$migration}.php" );

                \WP_CLI::success(
                    "Successfully ran the migration for '{$migration}'"
                );
            }
        }
        catch( \Throwable $e ) {
            \WP_CLI::error( "Migration failed: {$e->getMessage()}");
        }
    }

    /**
     * Rollback a specific migration.
     *
     * @since 1.0.0
     *
     * @param array $args.      The command line arguments from wp-cli
     *                          that do not have --
     * @param array $assoc_args The command line arguments from wp-cli
     *                          that have --
     *
     * @return void
     */
    public function rollback( $args, $assoc_args ) : void {
        if ( empty( $assoc_args ) || '' === $assoc_args['name'] ) {
            \WP_CLI::error( "The name of the migration must be provided must be provided as an argument (e.g. --name=MigrationClassName)" );
        }

        $name = $assoc_args['name'];
        try {
            if ( StringHelpers::string_ends_with( $name, ".php" ) ) {
                throw new \Exception(
                    "Please provide the migration name without the .php extension"
                );
            }

            $result = $this->migrator->rollback_migration( "{$name}.php" );

            \WP_CLI::success(
                "Successfully rolled back migration for {$result}"
            );
        }
        catch( \Throwable $e ) {
            \WP_CLI::error( "Rollback of {$name} failed: {$e->getMessage()}");
        }
    }
}
