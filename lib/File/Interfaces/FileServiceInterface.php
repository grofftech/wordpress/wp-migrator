<?php
/**
 * File Service Interface
 *
 * @package     Grofftech\WpMigrator\File\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\File\Interfaces;

/**
 * File Service interface.
 */
interface FileServiceInterface {
    function get_files( string $path );
}