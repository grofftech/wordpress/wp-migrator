<?php
/**
 *  File Service
 *
 * @package     Grofftech\WpMigrator\File
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\File;

use Grofftech\WpMigrator\Directory\Interfaces\DirectoryServiceInterface;
use Grofftech\WpMigrator\File\Interfaces\FileServiceInterface;
use Grofftech\WpMigrator\Service\Service;

/**
 * Directory Service class.
 */
class FileService extends Service implements FileServiceInterface {

    /**
     * The directory service object.
     *
     * @var DirectoryServiceInterface
     */
    private $directory_service;

    /**
     * Constructor.
     */
    public function __construct( DirectoryServiceInterface $directory_service )
    {
        $this->directory_service = $directory_service;
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() : void {

    }

    /**
     * Gets metadata for files.
     *
     * @param string $path The directory path the files exist in.
     *
     * @since 1.0.0
     *
     * @return array
     */
    public function get_files( string $path ) : array {
        $files = array();

        $directory = $this->directory_service->get_directory( $path );
        foreach ( $directory as $file ) {
            if ( $file->isDir() ) {
                continue;
            }

            $files['filename'][] = $file->getFilename();
            $files['filepath'][] = $file->getPathname();
        }

        return $files;
    }
}