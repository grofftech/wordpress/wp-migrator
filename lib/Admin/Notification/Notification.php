<?php
/**
 * Notification Handler
 *
 * @package     Grofftech\WpMigrator\Admin
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Admin\Notification;

/**
 * Notification class.
 */
class Notification {

    /**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Shows an error message by registering and firing a
     * custom action.
     *
     * @since 1.0.0
     *
     * @param string $message The error message to display.
     *
     * @return void
     */
    public function show_error_message( $message ) {
        // Add a custom action so we can use the message parameter.
        add_action(
            'wp-migrator-error-message',
            [ $this, 'render_error_message' ],
            10,
            1
        );

        // Do the custom action with the message.
		add_action( 'admin_notices', function() use ( $message ) {
            do_action( 'wp-migrator-error-message', $message );
        } );
    }

    /**
     * Renders the error message.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function render_error_message( $message ) {
        include __DIR__ . '/Views/Error.php';
    }
}