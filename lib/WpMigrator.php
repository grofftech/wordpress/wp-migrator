<?php
/**
 * Wp Migrator
 *
 * @package     Grofftech\WpMigrator
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator;

use Grofftech\WpMigrator\CLI\Migrate;
use Grofftech\WpMigrator\DateTime\DateTimeService;
use Grofftech\WpMigrator\Directory\DirectoryService;
use Grofftech\WpMigrator\File\FileService;
use Grofftech\WpMigrator\Domain\Migrator\Migrator;
use Grofftech\WpMigrator\Reflection\ReflectionService;
use Grofftech\WpMigrator\Service\ServiceRegistrar;

class WpMigrator extends ServiceRegistrar
{
    /**
     * The directory path of the plugin.
     *
     * @var string
     */
    private $plugin_path;

    /**
     * The url of the plugin.
     *
     * @var string
     */
    private $plugin_url;

    /**
     * The auryn dependency injector.
     *
     * Used by the parent to do the instantiations.
     *
     * @var Grofftech\WpMigrator\Dependencies\Auryn
     */
    protected $injector;

    /**
     * Constructor
     *
     * @param string $plugin_path The path to the plugin.
     * @param string $plugin_url  The plugin url.
     * @param Grofftech\WpMigrator\Dependencies\Auryn  $injector The dependency injector.
     */
    public function __construct( $plugin_path, $plugin_url, $injector )
    {
        $this->plugin_path = $plugin_path;
        $this->plugin_url = $plugin_url;
        $this->injector = $injector;
    }

    /**
     * Classes to be instantiated.
     *
     * @var array
     */
    protected $classes = array(
        DirectoryService::class,
        FileService::class,
        ReflectionService::class,
        DateTimeService::class,
        Migrator::class,
        Migrate::class,
    );

    /**
     * Interfaces to be aliased.
     */
    protected $interfaces = array(
        'Grofftech\WpMigrator\File\Interfaces\FileServiceInterface' => 'Grofftech\WpMigrator\File\FileService',

        'Grofftech\WpMigrator\Directory\Interfaces\DirectoryServiceInterface' => 'Grofftech\WpMigrator\Directory\DirectoryService',

        'Grofftech\WpMigrator\Domain\Interfaces\MigrationRepositoryInterface' => 'Grofftech\WpMigrator\Repositories\Migration\MigrationRepository',

        'Grofftech\WpMigrator\Reflection\Interfaces\ReflectionServiceInterface' => 'Grofftech\WpMigrator\Reflection\ReflectionService',

        'Grofftech\WpMigrator\DateTime\Interfaces\DateTimeServiceInterface' => 'Grofftech\WpMigrator\DateTime\DateTimeService'
    );

    /**
    * Kicks off plugin functionality.
    *
    * @since 1.0.0
    *
    * @return void
    */
    public function run() : void {
        // Instantiate classes for dependency injection.
        $this->init_constants();
        $this->register_hooks();
        parent::run();
    }

    /**
    * Initialize plugin constants.
    *
    * @since 1.0.0
    *
    * @return void
    */
    private function init_constants() : void {
        if ( ! defined( 'WP_MIGRATOR_DIR' ) ) {
            define( 'WP_MIGRATOR_DIR', $this->plugin_path );
        }

        if ( ! defined( 'WP_MIGRATOR_URL' ) ) {
            define( 'WP_MIGRATOR_URL', $this->plugin_url );
        }

        if ( ! defined( 'WP_MIGRATOR_NAME' ) ) {
            define( 'WP_MIGRATOR_NAME', 'Wp Migrator' );
        }

        if ( ! defined( 'WP_MIGRATOR_VERSION' ) ) {
            define( 'WP_MIGRATOR_VERSION', '1.0.0' );
        }

        if ( ! defined( 'WP_MIGRATOR_TEXT_DOMAIN' ) ) {
            define( 'WP_MIGRATOR_TEXT_DOMAIN', 'wp-migrator' );
        }
    }

    /**
    * Register hooks.
    *
    * @since 1.0.0
    *
    * @return void
    */
    public function register_hooks() {

    }

    /**
     * Runs anything that needs to be done when
     * the plugin is activated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function activate() : void {
        Migrator::create_migrations_table();
    }

    /**
     * Runs anything that needs to be done when
     * the plugin is deactivated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function deactivate() : void {

    }

    /**
     * Runs anything that needs to be done
     * when the plugin is uninstalled.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function uninstall() : void {
        Migrator::drop_migrations_table();
    }

    /**
     * Gets a specific class instance.
     *
     * @since 1.0.0
     *
     * @param string $classname The namespace with class name.
     *
     * @return object
     */
    public function get_class( string $classname ) : object {
        return (object) $this->classes[ $classname ];
    }
}