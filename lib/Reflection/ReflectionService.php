<?php
/**
 * Reflection Service
 *
 * @package     Grofftech\WpMigrator\Reflection
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Reflection;

use Grofftech\WpMigrator\Reflection\Interfaces\ReflectionServiceInterface;
use Grofftech\WpMigrator\Service\Service;
use ReflectionClass;

/**
 * Reflection Service class
 */
class ReflectionService extends Service implements ReflectionServiceInterface {

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() : void {

    }

    /**
     * Gets the namespace of a class file by using tokens.
     *
     * @param string $path_with_filename The path and filename (e.g. /lib/
     *                                   Filename.php)
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function get_namespace_from_class_file( $path_with_filename ) : string {
        $namespace = "";
        $tokens = token_get_all( file_get_contents( $path_with_filename ) );

        $token_count = count( $tokens );
        for ( $i = 0; $i < $token_count; $i++ ) {
            if ( is_array( $tokens[$i] ) ) {
                $token_name = token_name($tokens[$i][0]);

                if ( 'T_NAMESPACE' === $token_name ) {
                    $i += 2;
                    while ( isset($tokens[$i] ) && is_array( $tokens[$i] ) ) {
                        $namespace .= $tokens[$i++][1];
                    }
                    break;
                }
            }
        }

        return $namespace;
    }

    /**
     * Get class using reflection.
     *
     * @since 1.0.0
     *
     * @param string|object $class The class name as a string or the object.
     *
     * @return ReflectionClass
     */
    public function get_reflection_class( $class ) : ReflectionClass {
        return new ReflectionClass( $class );
    }
}