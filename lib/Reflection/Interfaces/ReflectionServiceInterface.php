<?php
/**
 * Reflection Service Interface
 *
 * @package     Grofftech\WpMigrator\Reflection
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Reflection\Interfaces;

/**
 * Reflection Service interface.
 */
interface ReflectionServiceInterface {
    public function get_namespace_from_class_file( $path_with_filename );
    public function get_reflection_class( $class );
}