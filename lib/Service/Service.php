<?php
/**
 * Service
 *
 * @package     Grofftech\PluginTemplate\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Service;

use Grofftech\WpMigrator\Interfaces\Hookable;
use Grofftech\WpMigrator\Interfaces\Runnable;

/**
 * Service class.
 */
abstract class Service implements Runnable, Hookable {
    /**
     * Run.
     */
    public function run() {
        $this->register_hooks();
    }
}