<?php
/**
 * Registers services required for the plugin.
 *
 * @package     Grofftech\WpMigrator\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Service;

use Grofftech\WpMigrator\Interfaces\Runnable;

/**
 * Service Registrar class
 */
abstract class ServiceRegistrar implements Runnable {

    /**
     * The classes to instantiate.
     *
     * @var array
     */
    protected $classes = array();

    /**
     * The interfaces to alias
     */
    protected $interfaces = array();

    /**
     * The auryn dependency injector.
     *
     * @var Auryn
     */
    protected $injector;

    /**
     * Runs initialization.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->init_interfaces();
        $this->register_services();
    }

    /**
     * Initializes interface alias for dependency injection.
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function init_interfaces() {
        foreach ( $this->interfaces as $interface => $class_name ) {
            $this->injector->alias( $interface, $class_name );
        }
    }

    /**
     * Registers services (classes & interfaces) by
     * instantiating them and calling their run methods.
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function register_services() {
        $this->classes = $this->init_classes();

        foreach ( $this->classes as $className => $object ) {
            if ( 'Grofftech\WpMigrator\CLI\Migrate' === $className ) {
                $object::init();
                continue;
            }

            $object->run();
        }
    }

    /**
     * Initializes the classes
     *
     * @since 1.0.0
     *
     * @return array An associative array of classes with a
     * namespace and the instantiated class.
     */
    protected function init_classes() {
        $objects = array_map( function( $class ) {
            return array(
                'namespace' => $class,
                'object' => $this->injector->make( $class ),
            );
        },
        $this->classes );

        return array_column( $objects, 'object', 'namespace');
    }
}