<?php
/**
 * Directory Service
 *
 * @package     Grofftech\WpMigrator\File
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Directory;

use DirectoryIterator;
use Grofftech\WpMigrator\Directory\Filters\DirectoryDotFilter;
use Grofftech\WpMigrator\Directory\Interfaces\DirectoryServiceInterface;
use Grofftech\WpMigrator\Service\Service;

/**
 * Directory Service class.
 */
class DirectoryService extends Service implements DirectoryServiceInterface {

    /**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() : void {

    }

    /**
     * Get the directory as a directory iterator.
     *
     * @param string $path The path to the directory.
     *
     * @since 1.0.0
     *
     * @return DirectoryDotFilter
     */
    public function get_directory( $path ) : DirectoryDotFilter {
        return new DirectoryDotFilter(
            new \DirectoryIterator( $path )
        );
    }
}