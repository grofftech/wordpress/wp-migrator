<?php
/**
 * Directory Dot Filter
 *
 * @package     Grofftech\WpMigrator\Directory\Filters
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Directory\Filters;

use DirectoryIterator;
use FilterIterator;

/**
 * Directory Dot Filter class.
 */
class DirectoryDotFilter extends FilterIterator {

    /**
     * Constructor
     */
    public function __construct( DirectoryIterator $directory_iterator ) {
        parent::__construct( $directory_iterator );
    }

    /**
     * The accept method for the directory iterator.
     * Filters out values.
     *
     * @since 1.0.0
     *
     * @return bool
     */
    public function accept() {
        return !$this->getInnerIterator()->isDot();
    }
}