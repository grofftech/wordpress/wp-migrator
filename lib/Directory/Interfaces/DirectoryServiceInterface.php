<?php
/**
 * Directory Service Interface
 *
 * @package     Grofftech\WpMigrator\File\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\WpMigrator\Directory\Interfaces;

/**
 * Directory Service interface.
 */
interface DirectoryServiceInterface {
    function get_directory( $path );
}