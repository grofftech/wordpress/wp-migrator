# WPMigrator

WPMigrator is a WordPress plugin for allowing database migrations to be done with code.

## Requirements

- Composer version 2+ [https://getcomposer.org/](https://getcomposer.org/)
- WPCLI [https://wp-cli.org/](https://wp-cli.org/)

## Installation

Installing this plugin assumes you are managing plugins for a WordPress site with `composer` or `wp-packagist`. It is currently not part of the WordPress plugin repository so it can't be installed with `wp-packagist`.

Add the following configuration to your `composer.json`. If you are managing plugins with `wp-packagist`, this configuration may already be present.

```json
    "extra": {
        "installer-paths": {
            "plugins/{$name}/": ["type:wordpress-plugin"]
        }
    }
```

Install with composer

```bash
composer require grofftech\wp-migrator
```

## Usage

This plugin is designed to be used in conjunction with another plugin, which hooks into the file paths to look for classes to perform the migrations. Ideally, the migrations can be performed with WordPress API methods. You can definitely use SQL to perform the migrations, with the recommendation usage of the global $wpdb object [https://developer.wordpress.org/reference/classes/wpdb/](https://developer.wordpress.org/reference/classes/wpdb/).

### Hooking into File Paths

Object orientated approach

```php
\add_filter('wp_migrator_paths', array( $this, 'add_migrations_folder' ) );
public function add_migrations_folder() {
    $paths[] = 'directory/for/migration/files'; # Could use a constant here

    return $paths;
}
```

Functional programming approach

```php
\add_filter('wp_migrator_paths', 'add_migrations_folder' );
public function add_migrations_folder() {
    $paths[] = 'directory/for/migration/files'; # Could use a constant here

    return $paths;
}
```

### Adding a Migration

Create a migration class (any class name is fine) that defines a run method. A namespace is required. The migration class(es) must be autoloaded (with composer or something similar) or included/required before the migration is run.

```php
<?php
namespace Grofftech\Migrations

class TestMigration {
    public function run() {
        // Code goes here
    }
}
```

Using WP-CLI, either run all pending migrations or run the migration by class name. Each time a migration is run, a record is saved to the database indicating it has been run.

```bash
wp migrate run # will run all pending migration classes not run before
wp migrate run --name=TestMigration
```

### Rolling Back a Migration

In order to rollback a migration, define the rollback method in the migration class.

```php
namespace Grofftech\Migrations;

class TestMigration {
    public function run() {
        // Code goes here
    }

    public function rollback() {
        // Rollback code here
    }
}
```

Using WP-CLI, rollback the migration using the class name. Rollbacks can only be performed on a previously run migration. If there is no rollback method the rollback will not run.

```bash
wp migrate rollback --name=TestMigration
```

## Contributing

Pull requests welcome. Please open an issue so we can discuss.

## License

GNU General Public License 2.0+
